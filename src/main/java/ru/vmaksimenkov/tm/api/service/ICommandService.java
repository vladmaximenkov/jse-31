package ru.vmaksimenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void add(@NotNull AbstractCommand command);

    @Nullable
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<AbstractCommand> getArgsCommands();

    @Nullable
    AbstractCommand getCommandByArg(@Nullable String arg);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @Nullable
    Collection<AbstractCommand> getCommands();

    @Nullable
    Collection<String> getListArgumentName();

    @Nullable
    Collection<String> getListCommandName();

}
