package ru.vmaksimenkov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.Domain;
import ru.vmaksimenkov.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class BackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public final static String NAME = "backup-load";

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @Nullable
    @Override
    public String commandDescription() {
        return "Load data from backup";
    }

    @NotNull
    @Override
    public String commandName() {
        return NAME;
    }

    @Override
    public @Nullable Role[] commandRoles() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        final File file = new File(BACKUP_XML);
        if (!file.exists()) return;
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(BACKUP_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

}
