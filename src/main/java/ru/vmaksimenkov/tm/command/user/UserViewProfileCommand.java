package ru.vmaksimenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show user profile";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-view";
    }

    @Override
    public void execute() {
        @Nullable final User user = serviceLocator.getAuthService().getUser();
        if (user == null) return;
        showUser(user);
    }

}
